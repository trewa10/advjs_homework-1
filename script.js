/*
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
    Коли ми створюємо об'єкт за допомогою конструктора, то він наслідує методи конструктора (отримає доступ до них). 
    Зберігаються такі методи у властивості [[Prototype]], яку має кожен об'єкт. __proto__ дозволяє отримати доступ до властивості [[Prototype]].
    Також можна додати нові методи безпосередньо до існуючого об'єкту з іншого за допомогою Object.getPrototypeOf() або __proto__ (застаріле).
    Додати новий метод до функції-конструктора можна через .prototype.newMethod().

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
    Щоб додати нові значення у класі-нащадку, йому також потрібно задати власний конструктор. Якщо його не задавати, автоматично створиться 
    пустий конструктор, який викличе батьківський конструктор і передасть йому аргументи. 
    У разі коли у класу-нащадка є власний конструктор, у ньому до використання this необхідно задати super(), що викличе батьківський конструктор
    і передасть йому аргументи.  
    Інакше отримаємо помилку щодо this, оскільки конструктор класу-нащадка не створює новий об'єкт і не присвоює this, а це робить
    батьківський конструктор, для виклику якого і потрібен super().
*/

"use strict";

class Employee {
    // не використовую в конструкторі нижнє підкреслення _, щоб не задавати властивості безпосередньо, а викликати одразу сеттери
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(value) {
        this._name = value;
    }

    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }

    get age() {
        return this._age;
    }

    set salary(value) {
        this._salary = value;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get lang() {
        return this._lang;
    }

    // видає помилку, якщо задавати лише геттер, тому довелось скопіювати і сеттер
    set salary(value) {
        super.salary = value;
    }

    get salary() {
        return super.salary * 3;
    }
}




const junior = new Programmer("Ivan", 27, 500, ["HTML", "CSS", "JavaScript", "React"]);
console.log(junior);

const middle = new Programmer("Petro", 22, 1500, ["HTML", "CSS", "JavaScript", "React", "Node.js"]);
console.log(middle);

const senior = new Programmer("Stepan", 31, 3000, ["HTML", "CSS", "JavaScript", "React", "Node.js", "TypeScript"]);
console.log(senior);
